## e2e test for Modus Create budgeting app
This directory contains e2e test scenarios for Modus Create budgeting app.
More instructions about the app available at: https://github.com/ModusCreateOrg/budgeting-sample-app-webpack2

## Features to test
* Adding new item
* Proper inflow and outflow updates when new item is added
* Consistency of inflow and outflow across the whole application
* Consistency of outflow categories across the whole application
* Items persistence

## Approach
Tests will be executed in automated way. Additional manual testing can be done to quickly identify
if graphs in “Reports” tab somehow correspond with the actual values (Graphs are generated properly).

## Test scenarios

#### User is able to add item with category, description and value
**Given** user is on “Budget” tab

**When** user add item with category “Travel” description “Flight to Morocco” and value “500”

**And** user hit “Add” button

**Then** new item is added with category “Travel” description “Flight to Morocco” and value “500”

#### User is able to add item without description
**Given** user is on “Budget” tab

**When** user add item with category “Travel” description “” and value “500”

**And** user hit “Add” button

**Then** new item is added with category “Travel” description “Flight to Morocco” and value “500”

#### User is not able to add item without value
**Given** user is on “Budget” tab

**When** user add item with category “Travel” description “” and value “”

**Then** user is not able to add the item

#### User is able to see added items after browser refresh
**Given** user is on “Budget” tab

**And** user add item with category “Travel” description “Flight to Morocco” and value “500”

**And** user hit “Add” button

**And** new item is added with category “Travel” description “Flight to Morocco” and value “500”

**When** user refreshes the browser

**Then** item is visible with category “Travel” description “Flight to Morocco” and value “500”

#### Total inflow is increased when user add income item
**Given** user is on “Budget” tab

**When** user add item with category “Income” description “” and value “500”

**And** user hit “Add” button

**Then** “Total Inflow” is increased by the value of the item

#### Total outflow is increased when user add outflow item
**Given** user is on “Budget” tab

**When** user add item with category “Travel” description “” and value “500”

**And** user hit “Add” button

**Then** “Total Outflow” is increased by the value of the item

#### Inflow in reports and budget has the same value
**When** “Total Inflow” has any value on “Budget” tab

**Then** “Total Inflow” has the same value on “Reports” tab

#### Outflow in reports and budget has the same value
**When** “Total outflow” has any value on “Budget” tab

**Then** “Total outflow” has the same value on “Reports” tab

#### All outflow categories are listed in "Inflow vs Outflow" tab
**Given** user is on “Budget” tab

**When** there are any outflow categories in items list

**Then** the same categories are listed in “Reports” on “Inflow vs Outflow”

#### All outflow categories are listed in "Spending by Category" tab
**Given** user is on “Budget” tab

**When** there are any outflow categories in items list

**Then** the same categories are listed in “Reports” on “Spending by Category”

## Key technologies used
* Cucumber
* Watir
* Ruby
* Rake
* FigNewton

## Prerequisities
* Ruby installed
* Instance of Modus Create budgeting app running

## Dependencies
To install all dependencies run
`rake dependencies`

## Execution
Use rake to run the whole suite with browser and report format specified
E.g to run tests in chrome browser with html report run
`rake test_all[chrome,html]`

To write report to standard output run
`rake test_all[chrome,pretty]`
