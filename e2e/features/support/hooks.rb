Before do
  browser = ENV['BROWSER'].nil? ? 'chrome' : ENV['BROWSER']
  case browser
    when 'chrome'
      # Forcing to use project binary so you don't have to download and setting it manually
      Selenium::WebDriver::Chrome.driver_path = "#{Dir.pwd}/drivers/chromedriver.exe"
      @browser = Watir::Browser.new :chrome
    when 'firefox'
      @browser = Watir::Browser.new :firefox
    else
      fail "Wrong browser type #{browser}"
  end
  init_pages(@browser)
end

After do |scenario|
  if scenario.failed?
    img = @browser.driver.screenshot_as(:base64)
    embed("data:image/png;base64,#{img}",'image/png')
  end
  @browser.close
end

private
def init_pages(browser)
  @navigation = Navigation.new(browser)
  @item_list = ItemList.new(browser)
end
