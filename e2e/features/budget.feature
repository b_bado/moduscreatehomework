Feature: Budget
  Test correct functionality of actions related to budget
  User should be able to:
    • Add new item
    • See inflow and outflow updates when new item is added
    • See added items after revisiting application (browser refresh etc.)

  Scenario: User is able to add item with category, description and value
    Given user is on "Budget" tab
    When user add item with category "Travel" description "Flight to Morocco" and value "500"
    And user hit "Add" button
    Then user list has item with category "Travel" description "Flight to Morocco" and value "500"

  Scenario: User is able to add item without description
    Given user is on "Budget" tab
    When user add item with category "Travel" description "" and value "500"
    And user hit "Add" button
    Then user list has item with category "Travel" description "" and value "500"

  Scenario: User is not able to add item without value
    Given user is on "Budget" tab
    When user add item with category "Travel" description "Flight To Morocoo" and value ""
    Then user is not able to add the item

  # Currently failing - items are not properly persisted and browser refresh cause all added items to disappear
  Scenario: User is able to see added items after browser refresh
    Given user is on "Budget" tab
    And user add item with category "Travel" description "Flight to Morocco" and value "500"
    And user hit "Add" button
    When user refreshes browser
    Then user list has item with category "Travel" description "Flight to Morocco" and value "500"
