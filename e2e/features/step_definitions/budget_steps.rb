Given(/^user is on "(.+)" tab$/) do |tab|
  @browser.goto(BASE_URL)
  @navigation.select_tab(tab)
end

When(/^user add item with category "(.+)" description "(.*)" and value "(.*)"$/) do |category, description, value|
  @item_list.select_category category
  @item_list.set_description description
  @item_list.set_value value
end

And(/^user hit "Add" button$/) do
  @item_list.add
end

Then(/^user list has item with category "(.+)" description "(.*)" and value "(.*)"$/) do |category, description, value|
  fail "Item with category: #{category}, description: #{description} and amount: #{value} not present in item list and should be" unless @item_list.item_present? category, description, value.to_f
end


Then(/^user is not able to add the item$/) do
  fail 'User should not be able to add the item but he is' if @item_list.add_enabled?
end
