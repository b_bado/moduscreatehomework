# Represents navigation providing switching between tabs
class Navigation
  def initialize(browser)
    @navbar = browser.div(class: 'components-Header-style-header')
    @reports_tab = @navbar.link text: 'Reports'
    @budget_tab = @navbar.link text: 'Budget'
  end

  def select_tab(tab)
    case tab
      when 'Budget'
        @budget_tab.click
      when 'Reports'
        @reports_tab.click
      else
        fail "Unsupported tab #{tab}"
    end
  end
end
