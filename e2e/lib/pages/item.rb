# Represents one item in item list
class Item
  attr_reader :category, :amount, :description

  def initialize(parent)
    @category = parent.div(xpath: './td[1]/div[contains(@class, "cellContent")]').text
    @description = parent.div(xpath: './td[2]/div[contains(@class, "cellContent")]').text
    @amount = parent.div(xpath: './td[3]/div[contains(@class, "cellContent")]').text.gsub(/[^\d.]/, '').to_f
  end
end
