# Represents item list table containing various items
class ItemList
  def initialize(browser)
    @category_select = browser.select_list name: 'categoryId'
    @description_text_field = browser.text_field name: 'description'
    @value_number_field = browser.text_field name: 'value'
    @add_button = browser.button(text: 'Add')
    @items = browser.table(class: 'containers-BudgetGrid-style-budgetGrid')
  end

  # Sets category with given category
  def select_category(category)
    @category_select.select category
  end

  # Sets description with given description
  def set_description(description)
    @description_text_field.set description
  end

  # Sets the amount value with given value
  def set_value(value)
    @value_number_field.set value
  end

  # Clicks on add button
  def add
    @add_button.click
  end

  # Returns true if Add button is enabled
  # Returns false otherwise
  def add_enabled?
    @add_button.enabled?
  end

  # Return true if item with given category, description and value is present in item list.
  # Return false otherwise.
  def item_present?(category, description, value)
    get_items.each do |item|
      return true if item.category == category && item.description == description && item.amount == value
    end
    false
  end

  private
  # Convert items list into item objects
  def get_items
    @items.trs(xpath: './tbody/tr').map{ |i| Item.new(i) }
  end
end
